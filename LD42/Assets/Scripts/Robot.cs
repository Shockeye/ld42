﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//note: the player is also a robot. this class means a Bad Robot
public class Robot : MonoBehaviour
{
    public Level level;
    Rigidbody2D body;
    public float speed = 0.5f;
    Vector3 destination = Vector3.zero;

    enum RobotState{ idle, patroling, searching, chasing, shooting, dead}
    RobotState state = RobotState.idle;

    float viewRange = 5;
    float shootRange = 3;
    float viewAngle = 90;
    Vector2 viewDirection = Vector2.up;
    Transform target;

	// Use this for initialization
	void Start ()
    {
        body = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        Behavior();
        body.MovePosition(transform.position + (destination - transform.position).normalized * speed);

    }

    void Behavior()
    {
        if (state == RobotState.idle)
        {
            //find something to do
            target = Scan();
            if (target != null)
            {
                state = RobotState.chasing;
                destination = target.position;
                Debug.Log("targeting!");
                Debug.Log(target.name);
            }
            else
            {
                destination = transform.position;
            }
            
        }
        else if (state == RobotState.patroling)
        {
            // patrol the area
        }
        else if (state == RobotState.chasing)
        {
            //pursue - shoot when in range
            float distance = Vector2.Distance(transform.position, target.position);
            destination = target.position;
            if (distance < shootRange)
            {
                state = RobotState.shooting;
            }
            else
            if(distance > viewRange)
            {
                //lost target
                target = null;
                destination = transform.position;
                state = RobotState.idle;
            }
            
        }
        else if (state == RobotState.shooting)
        {
            //stop moving
            destination = transform.position;
            //take the shot
            float distance = Vector2.Distance(transform.position, target.position);
            Debug.Log("pew,pew!"); Debug.Log(distance);
            state = RobotState.idle;
        }




        }

    Transform Scan()
    {
        //scan area
        //float range = 3;
        Collider2D[] col = Physics2D.OverlapCircleAll(new Vector2(this.transform.position.x, this.transform.position.y), viewRange);
        for (int i = 0; i < col.Length; i++)
        {


            if (col[i].name == "Player")
            {
                destination = col[i].transform.position;

                //player in range, check visible
                Vector2 targetDir = (col[i].transform.position - transform.position).normalized;

                if (Vector2.Angle(transform.up, targetDir) < viewAngle / 2)
                {
                    //within FOV
                    //check for obstacles
                    float distance = Vector2.Distance(transform.position, col[i].transform.position);

                    RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, targetDir, distance);
                    bool occluded = false;
                    for (int h = 0; h < hit.Length; h++)
                    {
                        if (hit[h].collider.name != "Player" && hit[h].collider.gameObject != this.gameObject)
                        {

                            occluded = true;
                            
                        }
                    }

                    if(!occluded)return col[i].transform;
                }
            }
        }

        
        return null;
    }

    Vector2 AngleToDirection(float degrees)
    {
        degrees += transform.eulerAngles.z;
        return (new Vector2(Mathf.Sin(degrees * Mathf.Deg2Rad),Mathf.Cos(degrees * Mathf.Deg2Rad)));
    }
}
