﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//
//This will control the level
//
public class Level : MonoBehaviour
{
    public Texture2D mapTexture;
    public Transform playerPrefab;
    public Transform robotPrefab;
    public Transform wallPrefab;
    public Transform floorPrefab;
    public Transform hDoorPrefab;
    public Transform vDoorPrefab;
    public int width, height;
    Grid grid;
    int[,] map;
    public Map mapTest;
    /* notes
Vector3Int cellPosition = grid.WorldToCell(transform.position);
transform.position = grid.GetCellCenterWorld(cellPosition);*/

    // Use this for initialization
    void Start ()
    {

        grid = GetComponent<Grid>();
        ReadLevel();
        Buildlevel();

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void ReadLevel()
    {
        width = mapTexture.width;
        height = mapTexture.height;
        map = new int[width, height];
        for (int x = 0; x < mapTexture.width; x++)
        {
            for (int y = 0; y < mapTexture.height; y++)
            {
                
                Color pix = mapTexture.GetPixel(x, y);
                if (pix.a != 0)
                {
                    if (pix.r < 0.2f && pix.g < 0.2f && pix.b < 0.2f) //hacky fix for bug
                    {
                        map[x, y] = 1; //wall
                    }
                    else if (pix == Color.green)
                    {
                        
                        map[x, y] = 2;  //player spawn
                    }
                    else if (pix.r > 0.5f && pix.g < 0.5f && pix.b < 0.5f)// (pix == Color.red)
                    {
                       
                        map[x, y] = 3; // door
                    }
                    else if (pix == Color.blue)
                    {
                        
                        map[x, y] = 4; //enemy robot
                    }
                    else
                    {
                        
                        map[x, y] = 0;//empty floor
                    }

                }
                else
                {
                    map[x, y] = -1; //spaaaace
                }
            }
        }
    }

    void Buildlevel()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {

                if (!(map[x,y] < 0))// skip transparent pixels
                {
                    if (map[x,y] == 1) 
                    {
                        Vector3 position = grid.GetCellCenterWorld(new Vector3Int(x, y, -1));
                        Instantiate(wallPrefab, position, Quaternion.identity, this.transform);
                    }
                    else //all non black non transparent cells have floor
                    {
                        Vector3 position = grid.GetCellCenterWorld(new Vector3Int(x, y, 0));
                        Instantiate(floorPrefab, position, Quaternion.identity, this.transform);
                    }


                    if (map[x, y] == 2)
                    {
                        //spawn player
                        Vector3 position = grid.GetCellCenterWorld(new Vector3Int(x, y, -1));
                        Object player = Instantiate(playerPrefab, position, Quaternion.identity, this.transform);
                        player.name = "Player";
                    }
                    else
                    if (map[x, y] == 3)
                    {
                        //spawn door
                        Debug.Log("create door");
                        CreateDoor(x, y);
                    }
                    else
                    if (map[x, y] == 4)
                    {
                        //spawn robot enemy
                        Vector3 position = grid.GetCellCenterWorld(new Vector3Int(x, y, -1));
                        ///todo: rob't
                        Instantiate(robotPrefab, position, Quaternion.identity, this.transform);
                    }

                }
            }
        }
    }

    void SpawnPlayer()
    {
        //This is temporary doing it at level centre
        //later have spawn point set in editor
        Vector3 position = grid.GetCellCenterWorld(new Vector3Int(width/2, height/2, -1));
        Instantiate(playerPrefab, position, Quaternion.identity, this.transform);

    }


    void CreateDoor(int x, int y)
    {
        Vector3 position = grid.GetCellCenterWorld(new Vector3Int(x, y, -1));
        ///first check orientation 
        
        if(map[x-1,y] == 1 && map[x+1 , y] == 1)
        {
            //horizontal door
            Debug.Log("horizontal door");
            Object door = Instantiate(hDoorPrefab, position, Quaternion.identity, this.transform);
        }
        else if((map[x , y - 1] == 1 && map[x, y + 1] == 1))
        {

            //vertical sliding door
            Debug.Log("vertical door");
            Object door = Instantiate(vDoorPrefab, position, Quaternion.identity, this.transform);
        }
        else
        {
            Debug.Log("bad door");
        }
    }
}
