﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool horizontal;
    public Rigidbody2D doorPrefab;
    Rigidbody2D body;
    GameObject sensor1;
    GameObject sensor2;
    float speed = 1f;
    Vector3 closed;
    Vector3 opened;
    bool open = false;
    int users = 0;
    
    // Use this for initialization
    void Start ()
    {
        closed = transform.position;
        body = Instantiate(doorPrefab, closed, Quaternion.identity, this.transform) as Rigidbody2D;
        Rigidbody2D r = this.GetComponent<Rigidbody2D>();
        if(r == null)
        {
            Debug.Log("adding Rigidbody");
            r = this.gameObject.AddComponent<Rigidbody2D>();

        }

        r.gravityScale = 0;
        sensor1 = new GameObject();
        BoxCollider2D b1 = sensor1.AddComponent<BoxCollider2D>();
        b1.isTrigger = true;
        b1.tag = "Door";


        //position triggers
        if (horizontal)
        {
            opened = this.transform.position + new Vector3(-12, 0, 0);
            sensor1.transform.position = this.transform.position;
            sensor1.transform.localScale = new Vector3(1,3,1);




        }
        else
        {
            opened = this.transform.position + new Vector3( 0,-1, 0);


            sensor1.transform.position = this.transform.position;
            sensor1.transform.localScale = new Vector3( 3,1, 1);


        }
        sensor1.transform.parent = this.transform;
        
       

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void FixedUpdate()
    {
        Vector3 destination;
        if(open)
        {
            destination = opened;
        }
        else
        {
            destination = closed;
        }

        body.MovePosition(transform.position + (destination - transform.position).normalized * speed);

    }

    public void Open(bool open)
    {
        
        if(open)
        {
            users++;
            this.open = open;
        }
        else
        {
            users--;
            if (users < 1) this.open = false;
        }
        
        
    }



    
}
