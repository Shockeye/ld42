﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorUser : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("trigger");
        if (other.tag == "Door")
        {
            Door door = (other.gameObject.transform.parent).GetComponent<Door>();
            door.Open(true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Door")
        {
            Door door = (other.gameObject.transform.parent).GetComponent<Door>();
            door.Open(false);
        }
    }
}
