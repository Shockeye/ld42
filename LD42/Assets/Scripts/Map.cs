﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Map")]
public class Map : ScriptableObject
{
    public int width, height;
    int[,] map;
    private void Awake()
    {
        map = new int [width,height];
    }

}
