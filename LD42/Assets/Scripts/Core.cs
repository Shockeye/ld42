﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : MonoBehaviour
{

    private KeyCode HotKey_screenshot = KeyCode.F12;
    public Canvas menu;

    void Start()
    {

    }


    void Update()
    {
        if (Input.GetKeyDown(HotKey_screenshot))
        {
            Screenshot();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
                Quit();

        }
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void Screenshot()
    {
        ScreenCapture.CaptureScreenshot("Screenshot.png");
    }

     
}
