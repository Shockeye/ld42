﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : MonoBehaviour
{

    private KeyCode HotKey_screenshot = KeyCode.F12;
    public Canvas menu;

    void Start()
    {

    }


    void Update()
    {
        if (Input.GetKeyDown(HotKey_screenshot))
        {
            Screenshot();
        }

        if (Input.GetButton("Cancel"))
        {
                Quit();

        }
    }

    public void Begin()
    {
        //start the game...
        menu.GetComponent<Canvas>().enabled = false;
        Arena a = GetComponent<Arena>();
        a.Begin();
    }
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#elif (UNITY_WEBGL)
    Application.OpenURL("about:blank");
#else
        Application.Quit();
#endif
    }

    public void Screenshot()
    {
        ScreenCapture.CaptureScreenshot("Screenshot.png");
    }

     
}
