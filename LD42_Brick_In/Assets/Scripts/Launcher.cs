﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    public enum Direction { left, right}
    public Direction direction;
    public Rigidbody2D brick;
    public float power = 200;
    public float delay;
    float timer;
    bool launch;
	// Use this for initialization
	void Awake ()
    {
        timer = float.PositiveInfinity;

    }

    public void Begin()
    {
        SetTimer();
    }

    void Update()
    {
        
    }

    void FixedUpdate ()
    {
		if(Time.time > timer)
        {
            //launch next brick
            Launch();
            SetTimer();
        }
	}

    public void SetTimer()
    {
        timer = Time.time + delay;
        delay -= 0.01f; //speed up
        if (delay < 0.75f) delay = 0.75f;
    }

    void Launch()
    {
        Rigidbody2D b = Instantiate(brick, this.gameObject.transform);
        float force;
        int r = Random.Range(-50, 150);
        if(direction == Direction.right)
        {
            force = -Mathf.Abs(power + r);
        }
        else
        {
            force = Mathf.Abs(power+ r);
        }
        b.AddForce(new Vector2(force, 50));
    }
}
