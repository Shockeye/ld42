﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D body;
    public float move = 1; //movement scaling
    public float bounce = 300;
	// Use this for initialization
	void Start ()
    {
        body = GetComponent<Rigidbody2D>();
        body.gravityScale = 0;
	}
	
	
	void FixedUpdate ()
    {
        body.MovePosition(new Vector2(Mathf.Clamp(transform.position.x + (Input.GetAxis("Horizontal") * move),-6f,6f), transform.position.y));

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Brick")
        {
            Rigidbody2D rb =collision.gameObject.GetComponent<Rigidbody2D>();
            if (!rb.IsSleeping()) //don't bounce them one they land
            {
                rb.AddForce(new Vector2(0, bounce));
            }
        }
    }
}
