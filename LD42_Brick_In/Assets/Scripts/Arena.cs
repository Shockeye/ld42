﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    public Launcher left, right;
	// Use this for initialization
	void Start ()
    {

		
	}

    public void Begin()
    {
        left.Begin();
        right.Begin();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("destroyed brick");
        Destroy(collision.gameObject);
    }
}
